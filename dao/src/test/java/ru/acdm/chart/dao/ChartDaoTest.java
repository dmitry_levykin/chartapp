package ru.acdm.chart.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.acdm.chart.model.db.BuyerPurchase;
import ru.acdm.chart.model.db.ProductPurchase;
import ru.acdm.chart.model.db.Profit;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:conf/ChartDaoTest.xml")
public class ChartDaoTest {

    @Autowired
    private ChartDao chartDao;

    @Test
    public void getProfitTest() {
        Date dateStart = new GregorianCalendar(2015, 1, 1).getTime();
        Date dateEnd = new GregorianCalendar(2015, 1, 28).getTime();
        List<Profit> profitList = chartDao.getProfit(dateStart, dateEnd);
        Assert.assertNotNull(profitList);
        Assert.assertEquals(28, profitList.size());
    }

    @Test
    public void getTop20BuyersTest() {
        List<BuyerPurchase> buyerPurchaseList = chartDao.getTop20Buyers();
        Assert.assertNotNull(buyerPurchaseList);
        Assert.assertEquals(20, buyerPurchaseList.size());
    }

    @Test
    public void getTop20ProductsTest() {
        List<ProductPurchase> productPurchaseList = chartDao.getTop20Products();
        Assert.assertNotNull(productPurchaseList);
        Assert.assertEquals(10, productPurchaseList.size());
    }
}

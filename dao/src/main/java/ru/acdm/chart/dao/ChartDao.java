package ru.acdm.chart.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import ru.acdm.chart.model.db.BuyerPurchase;
import ru.acdm.chart.model.db.ProductPurchase;
import ru.acdm.chart.model.db.Profit;

import java.util.Date;
import java.util.List;

@Repository
public interface ChartDao {
    /**
     * Сумма выручки по дням за период без учета времени
     */
    @Select("select " +
            "   cast(d.paytime as date) as date, " +
            "   count(*) as purchaseCount, " +
            "   sum(price) as value " +
            "from " +
            "   deal d, product p " +
            "where " +
            "   d.product_id = p.id " +
            "   and d.paytime >= cast(#{startDate} as date) " +
            "   and cast(d.paytime as date) <= cast(#{endDate} as date) " +
            "group by cast(d.paytime as date) "+
            "order by date")
    public List<Profit> getProfit(@Param("startDate")Date startDate, @Param("endDate")Date endDate);

    /**
     * ТОП-20 покупателей по сумме затрат на покупки
     */
    @Select("select " +
            "   b.id, " +
            "   b.name, " +
            "   count(*) as purchaseCount, " +
            "   sum(p.price) as purchaseSum " +
            "from " +
            "   deal d, buyer b, product p " +
            "where " +
            "   d.buyer_id = b.id " +
            "   and d.product_id = p.id " +
            "group by b.id, b.name " +
            "order by purchaseSum desc "+
            "limit 20")
    public List<BuyerPurchase> getTop20Buyers();

    /**
     * ТОП-20 товаров по сумме затрат на покупки
     */
    @Select("select " +
            "   p.id, " +
            "   p.name, " +
            "   count(*) as purchaseCount, " +
            "   sum(p.price) as purchaseSum " +
            "from " +
            "   deal d, product p " +
            "where " +
            "   d.product_id = p.id " +
            "group by p.id, p.name " +
            "order by purchaseSum desc "+
            "limit 20")
    public List<ProductPurchase> getTop20Products();
}

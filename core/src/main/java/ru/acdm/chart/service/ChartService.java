package ru.acdm.chart.service;

import ru.acdm.chart.model.db.BuyerPurchase;
import ru.acdm.chart.model.db.ProductPurchase;
import ru.acdm.chart.model.db.Profit;

import java.util.List;

public interface ChartService {
    /**
     * Сумма выручки по дням за последние 30 суток без учета времени
     */
    public List<Profit> getLast30DaysProfit();

    /**
     * ТОП-20 покупателей по сумме затрат на покупки
     */
    public List<BuyerPurchase> getTop20Buyers();

    /**
     * ТОП-20 товаров по сумме затрат на покупки
     */
    public List<ProductPurchase> getTop20Products();
}

package ru.acdm.chart.model.chart;

import java.util.HashMap;
import java.util.Map;

/**
 * График
 */
public class Chart {

    private String name;

    private Map<String, Object> data = new HashMap<>();

    public Map<String, Object> getData() {
        return data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

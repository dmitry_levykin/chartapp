package ru.acdm.chart.model.db;

import java.util.Date;

/**
 * Выручка
 */
public class Profit {

    private Date date;
    private Integer purchaseCount;
    private Float value;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }
}

package ru.acdm.chart.model.db;

/**
 * Покупатель
 *
 * См. таблицу buyer
 */
public class Buyer {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package ru.acdm.chart.model.db;

/**
 * Попупатель с суммой затрат на покупки
 */
public class BuyerPurchase extends Buyer {

    private Integer purchaseCount;
    private Float purchaseSum;

    public Integer getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    public Float getPurchaseSum() {
        return purchaseSum;
    }

    public void setPurchaseSum(Float purchaseSum) {
        this.purchaseSum = purchaseSum;
    }
}

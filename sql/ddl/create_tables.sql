CREATE TABLE buyer (
    id integer NOT NULL,
    name character varying(500)
);

CREATE SEQUENCE buyer_id_seq
    START WITH 1
    INCREMENT BY 1;

CREATE TABLE deal (
    id integer NOT NULL,
    paytime timestamp without time zone,
    buyer_id integer,
    product_id integer
);

CREATE SEQUENCE deal_id_seq
    START WITH 1
    INCREMENT BY 1;

CREATE TABLE product (
    id integer NOT NULL,
    name character varying(500),
    price real
);

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1;

ALTER TABLE buyer ALTER COLUMN id SET DEFAULT nextval('buyer_id_seq');

ALTER TABLE deal ALTER COLUMN id SET DEFAULT nextval('deal_id_seq');

ALTER TABLE product ALTER COLUMN id SET DEFAULT nextval('product_id_seq');

ALTER TABLE buyer ADD CONSTRAINT buyer_primary_key PRIMARY KEY (id);

ALTER TABLE deal ADD CONSTRAINT deal_primary_key PRIMARY KEY (id);

ALTER TABLE product ADD CONSTRAINT product_primary_key PRIMARY KEY (id);

ALTER TABLE deal ADD CONSTRAINT deal_buyer_id_fkey FOREIGN KEY (buyer_id) REFERENCES buyer(id);

ALTER TABLE deal ADD CONSTRAINT deal_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);

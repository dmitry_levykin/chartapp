DROP TABLE deal;

DROP TABLE buyer;

DROP TABLE product;

DROP SEQUENCE deal_id_seq;

DROP SEQUENCE buyer_id_seq;

DROP SEQUENCE product_id_seq;

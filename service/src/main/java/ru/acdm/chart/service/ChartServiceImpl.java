package ru.acdm.chart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.acdm.chart.dao.ChartDao;
import ru.acdm.chart.model.db.BuyerPurchase;
import ru.acdm.chart.model.db.ProductPurchase;
import ru.acdm.chart.model.db.Profit;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class ChartServiceImpl implements ChartService {

    @Autowired
    private ChartDao chartDao;

    @Override
    public List<Profit> getLast30DaysProfit() {
        Date currentDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        return chartDao.getProfit(calendar.getTime(), currentDate);
    }

    @Override
    public List<BuyerPurchase> getTop20Buyers() {
        return chartDao.getTop20Buyers();
    }

    @Override
    public List<ProductPurchase> getTop20Products() {
        return chartDao.getTop20Products();
    }
}

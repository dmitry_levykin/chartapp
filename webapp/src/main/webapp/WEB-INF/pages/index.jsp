<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
    <script src="<c:url value="/js/highcharts-custom.js" />" type="text/javascript"></script>
    <script src="<c:url value="/js/app/chart.js" />" type="text/javascript"></script>
    <script>
        $(function() {
            Highcharts.setOptions({global: {useUTC: false}});
            reloadCharts();
        });
    </script>
</head>
<body>
    <div id="error"></div>
    <div id="last30DaysProfitChart"></div>
    <div id="top20BuyersChart" style="height: 600px"></div>
    <div id="top20ProductsChart" style="height: 600px"></div>
    <div style="text-align: right">
        <a href="https://bitbucket.org/dmitry_levykin/chartapp/src">Исходники</a>
    </div>
</body>
</html>

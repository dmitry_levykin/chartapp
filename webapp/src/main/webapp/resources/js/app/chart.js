// Получение данных для графиков и отрисовка
function reloadCharts() {
    $.ajax({url: 'getChartData',
        async: true,
        type: 'POST',
        contentType: 'application/json',
        success: function (response) {
            drawLast30DaysProfitChart($('#last30DaysProfitChart'), response.last30DaysProfitChart);
            drawTop20Chart($('#top20BuyersChart'), response.top20BuyersChart);
            drawTop20Chart($('#top20ProductsChart'), response.top20ProductsChart);
        },
        error: function (response) {
            $('#error').html("<h4>Ошибка получения даннных</h4>");
            console.log(response);
        }
    });
}

// Выручка за последние 30 дней
function drawLast30DaysProfitChart(element, chart) {
    element.highcharts({
        title: {text: chart.name},
        xAxis: {type: 'datetime', minorTickInterval: 1000*60*60*24},
        yAxis: {min: 0, title: {text: '$'}},
        legend: {enabled: false},
        tooltip: {formatter: function() {
            return Highcharts.dateFormat('%e %b %Y', new Date(this.x)) + '<br/><b>' + this.y + '$</b>';
        }},
        plotOptions: {series: {animation: false},
            line: {marker: {enabled: false}}},
        series: [{data: chart.data.series}]
    });
}

// Top 20 покупателей и Top 20 товаров
function drawTop20Chart(element, chart) {
    element.highcharts({
        chart: {type: 'bar'},
        title: {text: chart.name},
        xAxis: {categories: chart.data.categories, title: {text: ''}},
        yAxis: {min: 0, title: {text: '$'}},
        legend: {enabled: false},
        tooltip: {formatter: function() {
            return this.x + '<br/><b>' + this.y + '$</b>';
        }},
        plotOptions: {series: {animation: false}},
        series: [{data: chart.data.series}]
    });
}
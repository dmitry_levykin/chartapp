package ru.acdm.chart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.acdm.chart.model.chart.Chart;
import ru.acdm.chart.model.db.BuyerPurchase;
import ru.acdm.chart.model.db.ProductPurchase;
import ru.acdm.chart.model.db.Profit;
import ru.acdm.chart.service.ChartService;

import java.util.*;

@Controller
public class MainController {

    private final static String INDEX_PAGE = "index";

    @Autowired
    private ChartService chartService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome() {
        return INDEX_PAGE;
    }

    @RequestMapping(value = "/getChartData", method = RequestMethod.POST)
    public @ResponseBody Map<String, Object> getChartData() {
        List<Profit> last30DaysProfit = chartService.getLast30DaysProfit();
        List<BuyerPurchase> top20Buyers = chartService.getTop20Buyers();
        List<ProductPurchase> top20Products = chartService.getTop20Products();

        Map<String, Object> map = new HashMap<>();
        map.put("last30DaysProfitChart", buildLast30DaysProfitChart(last30DaysProfit));
        map.put("top20BuyersChart", buildTop20BuyersChart(top20Buyers));
        map.put("top20ProductsChart", buildTop20ProductsChart(top20Products));

        return map;
    }

    /**
     * Выручка за последние 30 дней
     */
    private Chart buildLast30DaysProfitChart(List<Profit> last30DaysProfit) {
        Chart chart = new Chart();
        chart.setName("Динамика выручки за последние 30 дней");
        // Вспомогательная структура
        Map<Date, Profit> dateProfitMap = new HashMap<>();
        for (Profit profit : last30DaysProfit) {
            dateProfitMap.put(profit.getDate(), profit);
        }
        // Если в БД на какой-либо день данных нет, то считаем выручку нулевой
        List<Date> dateList = getLast30Days();
        Object[][] points = new Object[dateList.size()][2];
        int counter = 0;
        for (Date date : dateList) {
            Profit profit = dateProfitMap.get(date);
            points[counter++] = new Object[]{date, profit == null ? 0 : profit.getValue()};
        }
        chart.getData().put("series", points);
        return chart;
    }

    /**
     * Лучшие 20 покупателей по сумме затрат на покупки
     */
    private Chart buildTop20BuyersChart(List<BuyerPurchase> top20Buyers) {
        Chart chart = new Chart();
        chart.setName("Top 20 покупателей");
        List<String> categories = new ArrayList<>(top20Buyers.size());
        List<Float> points = new ArrayList<>(top20Buyers.size());
        for (BuyerPurchase buyerPurchase : top20Buyers) {
            categories.add(buyerPurchase.getName());
            points.add(buyerPurchase.getPurchaseSum());
        }
        chart.getData().put("series", points);
        chart.getData().put("categories", categories);
        return chart;
    }

    /**
     * Лучшие 20 товаров по сумме затрат на покупки
     */
    private Chart buildTop20ProductsChart(List<ProductPurchase> top20Products) {
        Chart chart = new Chart();
        chart.setName("Top 20 товаров");
        List<String> categories = new ArrayList<>(top20Products.size());
        List<Float> points = new ArrayList<>(top20Products.size());
        for (ProductPurchase productPurchase : top20Products) {
            categories.add(productPurchase.getName());
            points.add(productPurchase.getPurchaseSum());
        }
        chart.getData().put("series", points);
        chart.getData().put("categories", categories);
        return chart;
    }

    /**
     * Список дат последних 30 дней
     */
    private List<Date> getLast30Days() {
        int days = 30;
        List<Date> dateList = new ArrayList<>(days);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        for (int i = 0; i <= days; i++) {
            dateList.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
        Collections.reverse(dateList);

        return dateList;
    }
}
